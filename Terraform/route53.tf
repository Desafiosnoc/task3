resource "aws_route53_zone" "domain" {
  name = "your domain"

  tags = {
    Environment = "your domain"
  }
}

resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.domain.zone_id
  name    = "www.your domain"
  type    = "A"
  ttl     = "300"
  records = [aws_eip.eip.public_ip]
}

resource "aws_route53_record" "blog" {
  zone_id = aws_route53_zone.domain.zone_id
  name    = "blog.your domain"
  type    = "A"
  ttl     = "300"
  records = [aws_eip.eip.public_ip]
}

resource "aws_route53_record" "loja" {
  zone_id = aws_route53_zone.domain.zone_id
  name    = "loja.your domain"
  type    = "A"
  ttl     = "300"
  records = [aws_eip.eip.public_ip]
}

resource "aws_route53_record" "tomcat" {
  zone_id = aws_route53_zone.domain.zone_id
  name    = "tomcat.your domain"
  type    = "A"
  ttl     = "300"
  records = [aws_eip.eip.public_ip]
}