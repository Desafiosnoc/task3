
#Create VPC
resource "aws_vpc" "vpc_terraform" {
  cidr_block = var.cidr
  # instance_tenancy     = var.instance_tenancy
  # enable_dns_hostnames = var.enable_dns_hostnames
  # enable_dns_support   = var.enable_dns_support
  # enable_classiclink   = var.enable_classiclink
  tags = {
    Name = var.tags
  }
}

# Create Internet Gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc_terraform.id
  tags = {
    Name = var.tags
  }
}

# Create a Subnet
resource "aws_subnet" "subnet_public" {
  availability_zone       = var.av_zona
  vpc_id                  = aws_vpc.vpc_terraform.id
  map_public_ip_on_launch = true
  cidr_block              = "10.0.1.0/24"
  tags = {
    Name = var.tags
  }
}

# Create Custom Route Table
resource "aws_route_table" "route-public" {
  vpc_id = aws_vpc.vpc_terraform.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  tags = {
    Name = var.tags
  }
}

resource "aws_route_table_association" "subnet_public" {
  subnet_id      = aws_subnet.subnet_public.id
  route_table_id = aws_route_table.route-public.id
}

# Create Security Group to allow ports 
resource "aws_security_group" "SG_webserver" {
  name        = "SG_webserver"
  description = "allowed from the service port to any IP"
  vpc_id      = aws_vpc.vpc_terraform.id

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  ingress {
    description      = "SSH Port 22 allowed from any IP"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "HTTP Port allowed from any IP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "HTTPS Port allowed from any IP"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "TOMCAT Port 8080 allowed from any IP"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    description = "egress rule from any IP"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}